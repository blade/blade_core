
# Blade Core

Blade Core is a set of changes, APIs, and scripts used for customizing Chromium to make Blade.

Follow [@diamondsbattle](https://twitter.com/diamondsbattle) on Twitter for important
announcements.

## Resources

- [Issues](https://github.com/brave/brave-browser/issues)
- [Releases](https://github.com/brave/brave-browser/releases)
- [Documentation wiki](https://github.com/brave/brave-browser/wiki)

## Community

You can ask questions and interact with the community in the following
locations:
- [Discord](https://discord.gg/XUjs9Rv)
